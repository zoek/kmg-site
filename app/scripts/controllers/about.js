'use strict';

/**
 * @ngdoc function
 * @name kmgSiteApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the kmgSiteApp
 */
angular.module('kmgSiteApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
