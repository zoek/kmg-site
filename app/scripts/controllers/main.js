'use strict';

/**
 * @ngdoc function
 * @name kmgSiteApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the kmgSiteApp
 */
angular.module('kmgSiteApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
